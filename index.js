const express = require('express');

const app = express();

const ActionCable = require('./node_modules/actioncable-nodejs/src/actioncable.js');

let cable_url = 'ws://localhost:3000/cable';

let cable = new ActionCable(cable_url, {
  // If you validate the origin on the server, you can set that here
  origin: 'http://localhost:8080',

  // Using headers with an API key for auth is recommended
  // because we dont have the normal browser session to authenticate with
  headers: {
    'X-Api-Key': 'someexampleheader'
  }
});

let subscription = cable.subscribe('HelloChannel', {
  connected() {
    console.log("connected");
  },

  disconnected() {
    console.log("disconnected");
  },

  rejected() {
    console.log("rejected");
  },

  received(data) {
    console.log("received");
    console.log(data['message']);
  }
});

app.listen(8080, ()=>{
  console.log('RUN SERVER')
})

app.get('/pendientes',function(req,res){
  subscription.perform("send_message", {message: 'bar'});
  res.send('hola')
})
